/// print the sum, average, product, smallest and largest of these numbers
#include <iostream> /// allows program to perform input and output

int
main() 
{
    int a, b, c;
    std::cout << "Type tree numbers: " << "\n";
    std::cin >> a >> b >> c;
    std::cout << "Sum is " << a +b +c << std::endl;
    std::cout << "Average is " << (a +b + c) / 3 << std::endl;
    std::cout << "Product is " <<  a * b * c << std::endl;
	
    ///smallest number
    int small = a; 
    if (b < small) {
        small = b;
    }
    if (c < small) {
        small = c;
    }	  
    std::cout << "Smallest number: " << small << std::endl;
    

    /// Largest number
    int large = a;
    if (b > large) {
        large = b;
    }
    if (c > large) {
        large = c;
    }
    std::cout << "Largest number: " << large << std::endl;

    return 0;
}

