    In any class that doesn't explicitly include a constructor, the compiler provides a default constructor that is, a constructor with no parameters.
    The default constructor provided by the compiler creates a object without giving any initial values to the object's data members. For data members that are objects of other classes, the default constructor implicitly calls each data member's default constructor to ensure that the data member is initialized properly.

