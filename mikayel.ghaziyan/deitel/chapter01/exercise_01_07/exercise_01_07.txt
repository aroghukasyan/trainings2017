Mikayel Ghaziyan
23/09/2017

Exercise 1.7


With the object-oriented programming (OOP) the programmers can build reusable software components that represent items of the real world such as cars, buildings etc.  In addition, with OOP building software became faster, more accurate and less expensive. 
C++ is very popular for many reasons. First, in comparison with C, it is a OOP language and it is portable. Second, and I think the most important, C++ is used for a wide-range of commercial applications. C++ is used for creating server-side applications, applications that run inside the browsers, applications that are used by Operational Systems (OS) such as Windows. C++ is also used for creating graphics, games, database software and so on. Moreover, it is a basement for other programming languages such as Java. Therefore, by mastering C++, a programmer can find far more interesting and advanced career than with any other languages.  A large-scale of jobs of interest can be available to a good C++ programmer. 

