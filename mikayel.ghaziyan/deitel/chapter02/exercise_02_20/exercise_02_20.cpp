/// Mikayel Ghaziyan
/// 19/10/2017
/// Exercise 2.20

#include <iostream>
	
	
/// Beginning of main function
int
main()
{   
    /// Obtaining the value of the radius
    int radius;
    std::cout << "Enter the radius of the circle: \n";
    std::cin >> radius;  
		        
    /// Checking the value`s validity
    if (radius < 0) {
        std::cout << "Error 1. Negative value. Try again!" << std::endl;

        return 1;
    }
    
    /// Displaying the result
    std::cout << "The circle's diameter is " << radius * 2 << std::endl;
    std::cout << "The circumference is " << 2 * 3.14159 * radius << std::endl;
    std::cout << "Area is " << 3.14159 * radius * radius << std::endl;

    return 0;
} /// End of the main function

/// End of the file

