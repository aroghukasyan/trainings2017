#include <iostream>

int
main()
{
    int number1;
    int number2;
    
    std::cout << "Enter two numbers: ";
    std::cin  >> number1 >> number2;
    
    if (0 == number2) {
        std::cout << "Second number shouldn't be 0, because the denominator cannot be zero!\n";
        return 1;
    }
   
    std::cout << number1 << " + " << number2 << " = " << (number1 + number2) << std::endl;
    std::cout << number1 << " - " << number2 << " = " << (number1 - number2) << std::endl;
    std::cout << number1 << " x " << number2 << " = " << (number1 * number2) << std::endl;
    std::cout << number1 << " / " << number2 << " = " << (number1 / number2) << std::endl;
    
    return 0;
}


