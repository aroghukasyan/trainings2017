#include <iostream>

int
main ()
{
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    
    std::cout << "\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n";
    
    return 0;
}
