The function prototype is the function declaration which tells the computer the name, return type, and parameters of the function.
The function definition tells the compiler what task the function should perform. A funcion definition cannot be called unless the function is declared. The function definition contains all the code of the function : the declarations and the statements, and first the function name, return type, and the parameter declarations 
The definition and prototype should agree at function name, parameters’ type and return type level, otherwise a compiling error occurs.
