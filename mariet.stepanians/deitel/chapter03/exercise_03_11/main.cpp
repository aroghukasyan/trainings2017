#include "gradeBook.hpp"
#include <iostream>

int main()
{
    std::string nameOfCourse;
    std::string nameOfInstructor;

    GradeBook myGradeBook(nameOfCourse, nameOfInstructor);
    
    std::cout << "Gradebook of Semester1 " << std::endl;
    std::cout << "\nPlease enter the Course name: ";
    std::getline(std::cin, nameOfCourse); 
    myGradeBook.setCourseName(nameOfCourse);
    
    std::cout << "Please enter Instructor's name: ";
    std::getline(std::cin, nameOfInstructor);
    std::cout << "" << std::endl;
    myGradeBook.setInstructorName(nameOfInstructor); 
    
    myGradeBook.displayMessage();
                                        
    return 0;
}
